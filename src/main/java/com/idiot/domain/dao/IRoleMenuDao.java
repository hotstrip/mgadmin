package com.idiot.domain.dao;

import com.idiot.domain.model.RoleMenu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/16.
 */
@Mapper
public interface IRoleMenuDao {
    //根据角色编号删除角色菜单关系信息
    @Delete("delete from T_ROLE_MENU where ROLE_ID = #{roleId}")
    public int deleteRoleMenusByRoleId(@Param("roleId") long roleId);

    //查询该角色所拥有的菜单
    @Select("select * from T_ROLR_MENU where ROLE_ID = #{roleId}")
    public List<RoleMenu> getRoleMenusByRoleId(@Param("roleId") long roleId);

    //新增
    public int insert(RoleMenu roleMenu);

    //根据roleId 和 menuId获取角色菜单信息 select * from T_ROLE_MENU where
    public RoleMenu getRoleMenuByRoleMenu(RoleMenu roleMenu);

    //删除
    @Delete("delete from T_ROLE_MENU where ROLE_MENU_ID = #{roleMenuId}")
    public int delete(@Param("roleMenuId") long roleMenuId);
}
