package com.idiot.domain.dao;

import com.github.pagehelper.Page;
import com.idiot.domain.model.Log;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2016/12/14.
 */
@Mapper
public interface ILogDao {
    //新增日志信息
    public int insert(Log log);

    //分页查询所有日志信息
    public Page<Log> getAllLogs(RowBounds rowBounds);

    //删除日志信息
    @Delete("delete from T_LOG where LOG_ID = #{logId}")
    public int delete(@Param("logId") long logId);
}
