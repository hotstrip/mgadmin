package com.idiot.domain.dao;

import com.idiot.domain.model.AgentGroup;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
@Mapper
public interface IAgentGroupDao {
    //分页查询所有的终端分组信息
    @Select("select * from T_AGENT_GROUP")
    public List<AgentGroup> getAllAgentGroups(RowBounds rowBounds);

    //新增
    public int insert(AgentGroup info);

    //修改
    public int update(AgentGroup info);

    //根据agentGroupId 获取终端分组信息
    @Select("select * from T_AGENT_GROUP where AGENT_GROUP_ID = #{agentGroupId}")
    public AgentGroup getAgentGroupByAgentGroupId(@Param("agentGroupId") long agentGroupId);

    //删除
    @Delete("delete from T_AGENT_GROUP where AGENT_GROUP_ID = #{agentGroupId}")
    public int delete(@Param("agentGroupId") long agentGroupId);

    //获取所有有效的终端分组信息
    @Select("select * from T_AGENT_GROUP where STATUS = 1")
    public List<AgentGroup> getAllValidAgentGroups();
}
