package com.idiot.domain.dao;

import com.idiot.domain.model.Role;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
@Mapper
public interface IRoleDao {
    //分页查询所有角色
    public List<Role> getAllRoles(RowBounds rowBounds);

    //新增角色  
    public int insert(Role info);

    //修改角色
    public int update(Role info);

    //根据roleId获取角色信息
    @Select("select * from T_ROLE where ROLE_ID = #{roleId}")
    public Role getRoleByRoleId(@Param("roleId") long roleId);

    //删除角色信息
    @Delete("delete from T_ROLE where ROLE_ID = #{roleId}")
    public int delete(@Param("roleId") long roleId);

    //根据userId获取角色信息
    public List<Role> getRolesByUserId(@Param("userId") long userId);

    //查询所有有效的角色信息
    @Select("select * from T_ROLE where status = 1")
    public List<Role> getAllValidRoles();
}
