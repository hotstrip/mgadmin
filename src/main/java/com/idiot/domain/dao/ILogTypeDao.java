package com.idiot.domain.dao;

import com.idiot.domain.model.LogType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/14.
 */
@Mapper
public interface ILogTypeDao {
    //获取所有有效日志类型
    @Select("select * from T_LOG_TYPE where status = 1")
    public List<LogType> getAllValidLogTypes();
}
