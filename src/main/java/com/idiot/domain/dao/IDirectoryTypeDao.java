package com.idiot.domain.dao;

import com.idiot.domain.model.DirectoryType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
@Mapper
public interface IDirectoryTypeDao {
    //获取所有内容类型
    @Select("select * from T_DIRECTORY_TYPE")
    public List<DirectoryType> getAllDirectoryTypes(RowBounds rowBounds);

    //获取内容类型信息
    @Select("select * from T_DIRECTORY_TYPE where DIRECTORY_TYPE_ID = #{directoryTypeId}")
    public DirectoryType getDirectoryTypeByDirectoryTypeId(@Param("directoryTypeId") long directoryTypeId);

    //新增
    public int insert(DirectoryType info);

    //修改
    public int update(DirectoryType info);

    //获取所有有效目录类型信息
    @Select("select * from T_DIRECTORY_TYPE where status = 1")
    public List<DirectoryType> getAllValidDirectoryTypes();
}
