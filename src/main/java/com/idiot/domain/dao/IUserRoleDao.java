package com.idiot.domain.dao;

import com.idiot.domain.model.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/16.
 */
@Mapper
public interface IUserRoleDao {
    //根据userId删除用户角色关系信息
    @Delete("delete from T_USER_ROLE where USER_ID = #{userId}")
    public int deleteUserRolesByUserId(@Param("userId") long userId);

    //根据userId查询该用户拥有的角色
    @Select("select * from T_USER_ROLE where USER_ID = #{userId}")
    public List<UserRole> getUserRolesByUserId(long userId);

    //新增
    public int insert(UserRole userRole);

    //删除
    @Delete("delete from T_USER_ROLE where USER_ROLE_ID = #{userRoleId}")
    public int delete(@Param("userRoleId") long userRoleId);

    //根据userId 和 roleId获取UserRole信息
    public UserRole getUserRoleByUserRole(UserRole userRole);
}
