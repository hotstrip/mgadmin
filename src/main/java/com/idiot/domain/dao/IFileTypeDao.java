package com.idiot.domain.dao;

import com.idiot.domain.model.FileType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
@Mapper
public interface IFileTypeDao {
    //获取所有内容类型
    @Select("select * from T_FILE_TYPE")
    public List<FileType> getAllFileTypes(RowBounds rowBounds);

    //获取内容类型信息
    @Select("select * from T_FILE_TYPE where FILE_TYPE_ID = #{fileTypeId}")
    public FileType getFileTypeByFileTypeId(@Param("fileTypeId") long fileTypeId);

    //新增
    public int insert(FileType info);

    //修改
    public int update(FileType info);
}
