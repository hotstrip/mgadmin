package com.idiot.domain.dao;

import com.github.pagehelper.Page;
import com.idiot.domain.model.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
@Mapper
public interface IUserDao {
    //根据用户名获取用户信息
    public User getUserByName(@Param("userName") String userName);

    //获取所有管理员用户
    public Page<User> getAllAdminUsers(RowBounds rowBounds);

    //新增用户
    public int insert(User info);

    //修改用户
    public int update(User info);

    //根据userId获取用户信息
    @Select("select * from T_USER where USER_ID = #{userId}")
    User getUserByUserId(@Param("userId") long userId);

    //删除用户信息
    @Delete("delete from T_USER where USER_ID = #{userId}")
    int delete(@Param("userId") long userId);

    //获取所有有效用户
    @Select("select * from T_USER where STATUS = 1")
    public List<User> getAllUsers();
}
