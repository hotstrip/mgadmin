package com.idiot.domain.dao;

import com.idiot.domain.model.FileInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/28.
 */
@Mapper
public interface IFileDao {
    //获取目录下的文件信息
    @Select("select * from T_FILE where DIRECTORY_ID = #{directoryId} and status = 1")
    public List<FileInfo> getFilesByDirectoryId(@Param("directoryId") long parentDirectoryId);

    //新增
    public int insert(FileInfo fileInfo);

    //修改
    public int update(FileInfo fileInfo);
}
