package com.idiot.domain.dao;

import com.idiot.domain.model.Directory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/27.
 */
@Mapper
public interface IDirectoryDao {
    //获取所有有效目录信息
    @Select("select * from T_DIRECTORY where status = 1 and IS_ROOT = 1 and PARENT_DIRECTORY_ID is null")
    public List<Directory> getAllValidDirectories();

    //新增
    public int insert(Directory info);

    //修改
    public int update(Directory info);

    //根据parentDirectoryId获取有效目录信息
    public List<Directory> getDirectoryByParentDirectoryId(@Param("parentDirectoryId") long parentDirectoryId);

    //根据目录 加载目录信息
    public Directory getDirectoryByDirectoryId(@Param("directoryId") long directoryId);
}
