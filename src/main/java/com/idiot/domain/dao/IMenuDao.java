package com.idiot.domain.dao;

import com.github.pagehelper.Page;
import com.idiot.domain.model.Menu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
@Mapper
public interface IMenuDao {
    //根据userId获取菜单
    public List<Menu> getMenusByUserId(@Param("userId") long userId);

    //根据菜单parentId获取子菜单
    @Select("select * from T_MENU where STATUS = '1' and PARENT_ID = #{parentId,jdbcType=BIGINT} order by MENU_INDEX")
    public List<Menu> getMenusByParenrId(@Param("parentId") long parentId);

    //获取该用户的一级菜单
    public List<Menu> getFirstLevelMenusByUserId(@Param("userId") long userId);

    //获取所有菜单信息  分页
    @Select("select * from T_MENU")
    public Page<Menu> getAllPageMenus(RowBounds rowBounds);

    //新增菜单信息
    public int insert(Menu info);

    //修改菜单信息
    public int update(Menu info);

    //根据menuId获取菜单信息
    @Select("select * from T_MENU where MENU_ID = #{menuId}")
    public Menu getMenuByMenuId(@Param("menuId") long menuId);

    //删除菜单信息
    @Delete("delete from T_MENU where MENU_ID = #{menuId}")
    public int delete(@Param("menuId") long menuId);

    //获取所有有效的菜单信息  不分页
    @Select("select * from T_MENU where STATUS = 1")
    public List<Menu> getAllMenus();

    //获取该roleId对应的菜单信息
    public List<Menu> getMenusByRoleId(@Param("roleId") long roleId);

    //根据roleId获取一级菜单信息
    public List<Menu> getFirstMenusByRoleId(@Param("roleId") long roleId);

    //获取所有一级菜单信息
    @Select("select * from T_MENU where PARENT_ID is null or PARENT_ID = ''")
    public List<Menu> getAllfirstMenus();
}
