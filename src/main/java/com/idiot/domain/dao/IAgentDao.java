package com.idiot.domain.dao;

import com.idiot.domain.model.Agent;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/23.
 */
@Mapper
public interface IAgentDao {
    //新增
    public int insert(Agent info);

    //修改
    public int update(Agent info);

    //根据agentId获取终端信息
    @Select("select * from T_AGENT where AGENT_ID = #{agentId}")
    public Agent getAgentByAgentId(@Param("agentId") long agentId);

    //删除
    @Delete("delete from T_AGENT where AGENT_ID = #{agentId}")
    public int delete(@Param("agentId")long agentId);

    //查询所有终端信息
    public List<Agent> getAllAgents(RowBounds rowBounds);
}
