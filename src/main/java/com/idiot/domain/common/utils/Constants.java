package com.idiot.domain.common.utils;

/**
 * Created by idiot on 2016/12/15.
 * @description 常量
 */
public class Constants {
    public static class ResponseCode {
        public static final int success = 0;
        public static final int error = 1;
    }

    public static class Regular {
        public static final String onlineUser = "onlineUser";
    }
}
