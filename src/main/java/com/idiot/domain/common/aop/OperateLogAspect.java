package com.idiot.domain.common.aop;

import com.idiot.domain.common.utils.Constants;
import com.idiot.domain.common.utils.Constants.Regular;
import com.idiot.domain.model.Log;
import com.idiot.domain.model.User;
import com.idiot.service.ILogService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by idiot on 2016/12/21.
 * @description 记录操作日志信息 切面
 */
@Aspect
@Component
public class OperateLogAspect {
    private static Logger logger = LoggerFactory.getLogger(OperateLogAspect.class);

    @Resource
    private ILogService logService;

    //定义切点  controller类下面的带有RequiresPermissions(requestmapping)注解的方法  (org.springframework.web.bind.annotation.RequestMapping)
    @Around("execution(public * com.idiot.web..*(..)) and @annotation(org.apache.shiro.authz.annotation.RequiresPermissions))")
    public Object doMain(ProceedingJoinPoint proceedingJoinPoint){
        logger.info("=======>开始记录操作记录");
        //初始化log
        Log log = new Log();

        //获取request对象
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();

        String url = request.getRequestURL().toString();
        String method = request.getMethod();
        String uri = request.getRequestURI();
        String queryString = request.getQueryString();

        logger.info("请求开始, 各个参数, url: {}, method: {}, uri: {}, params: {}", url, method, uri, queryString);

        Object obj = null;
        try {
            obj = proceedingJoinPoint.proceed();
            log.setResponseCode(Constants.ResponseCode.success + "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            log.setResponseCode(Constants.ResponseCode.error + "");
        }

        User user = (User) request.getSession().getAttribute(Regular.onlineUser);
        if(user != null){
            log.setUserId(user.getUserId());                    //设置操作人
            log.setRequestUrl(uri);                             //设置请求路径
            logService.addOperateLog(log);
        }
        logger.info("=======>记录操作记录结束");
        return obj;
    }

}
