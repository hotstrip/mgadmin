package com.idiot.domain.common.exceptions;

import com.idiot.domain.common.exceptions.model.ResponseInfo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @description 全局异常处理类
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        return mav;
    }

    /**
     * @description 自定义异常处理
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = MyException.class)
    @ResponseBody
    public ResponseInfo<String> exceptionHandler(HttpServletRequest req, MyException e) throws Exception {
        ResponseInfo<String> r = new ResponseInfo<>();
        r.setMessage(e.getMessage());
        r.setCode(ResponseInfo.ERROR);
        r.setData("操作出错......");
        r.setUrl(req.getRequestURL().toString());
        return r;
    }

}

