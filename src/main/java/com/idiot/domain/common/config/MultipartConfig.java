package com.idiot.domain.common.config;

import org.springframework.boot.autoconfigure.web.MultipartProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;

/**
 * Created by idiot on 2016/12/28.
 */

/**
 * @description 设置上传文件大小限制  单位kb
 */
@Configuration
public class MultipartConfig {
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartProperties multipartProperties = new MultipartProperties();
        multipartProperties.setEnabled(true);
        multipartProperties.setMaxFileSize(100 + "MB");
        return multipartProperties.createMultipartConfig();
    }
}
