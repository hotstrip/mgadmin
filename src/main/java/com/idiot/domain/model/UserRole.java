package com.idiot.domain.model;

/**
 * Created by idiot on 2016/12/13.
 */
public class UserRole extends BaseBean{

    private static final long serialVersionUID = 4161504996391679671L;

    /** 用户角色关系编号 */
    private long userRoleId;
    /** 用户编号 */
    private long userId;
    /** 角色信息编号 */
    private long roleId;

    public long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
