package com.idiot.domain.model;


/**
 * Created by idiot on 2016/12/13.
 */
public class DirectoryType extends BaseBean {

    private static final long serialVersionUID = -8150796591270799243L;

    /** 内容类型编号 */
    private long directoryTypeId;
    /** 类型名称 */
    private String directoryTypeName;
    /** 存放目录的盘符*/
    private String disk;
    /** 目录编码*/
    private String code;

    public long getDirectoryTypeId() {
        return directoryTypeId;
    }

    public void setDirectoryTypeId(long directoryTypeId) {
        this.directoryTypeId = directoryTypeId;
    }

    public String getDirectoryTypeName() {
        return directoryTypeName;
    }

    public void setDirectoryTypeName(String directoryTypeName) {
        this.directoryTypeName = directoryTypeName;
    }

    public String getDisk() {
        return disk;
    }

    public void setDisk(String disk) {
        this.disk = disk;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
