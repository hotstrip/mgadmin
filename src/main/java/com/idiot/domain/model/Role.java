package com.idiot.domain.model;

/**
 * Created by Administrator on 2016/12/13.
 */
public class Role extends BaseBean{

    private static final long serialVersionUID = -5804956518620171123L;

    /** 角色编号 */
    private long roleId;
    /** 角色名称 */
    private String roleName;

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
