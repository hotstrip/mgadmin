package com.idiot.domain.model;

/**
 * Created by idiot on 2016/12/13.
 */
public class UserAgentGroup extends BaseBean {

    private static final long serialVersionUID = 5246436198913523464L;

    /** 用户终端关系编号 */
    private long userAgentGroupId;
    /** 终端分组编号 */
    private long agentGroupId;
    /** 用户编号 */
    private long userId;

    public long getUserAgentGroupId() {
        return userAgentGroupId;
    }

    public void setUserAgentGroupId(long userAgentGroupId) {
        this.userAgentGroupId = userAgentGroupId;
    }

    public long getAgentGroupId() {
        return agentGroupId;
    }

    public void setAgentGroupId(long agentGroupId) {
        this.agentGroupId = agentGroupId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
