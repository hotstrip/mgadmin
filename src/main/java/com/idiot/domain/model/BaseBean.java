package com.idiot.domain.model;


import java.io.Serializable;

/**
 * Created by idiot on 2016/12/13.
 */
public class BaseBean implements Serializable {

    private static final long serialVersionUID = -1394243817452550119L;

    /** 状态 */
    private int status;
    /** 备注 */
    private String remark;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
