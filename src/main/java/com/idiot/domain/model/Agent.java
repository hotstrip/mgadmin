package com.idiot.domain.model;

import java.util.Date;

/**
 * Created by idiot on 2016/12/13.
 */
public class Agent extends BaseBean {

    private static final long serialVersionUID = -1254920639454151831L;

    /** 终端信息编号 */
    private long agentId;
    /** 终端分组编号 */
    private long agentGroupId;
    /** 终端名称 */
    private String agentName;
    /** 终端编号 */
    private String agentCode;
    /** 终端IP */
    private String agentIp;
    /** 创建时间 */
    private Date createTime;
    /** 首次心跳时间 */
    private Date firstHeartBeatTime;
    /** 开机时间 */
    private Date startTime;
    /** 关机时间 */
    private Date endTime;
    /** 终端软件版本 */
    private String csNo;
    /** 音量 */
    private int volume;

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public long getAgentGroupId() {
        return agentGroupId;
    }

    public void setAgentGroupId(long agentGroupId) {
        this.agentGroupId = agentGroupId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentIp() {
        return agentIp;
    }

    public void setAgentIp(String agentIp) {
        this.agentIp = agentIp;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getFirstHeartBeatTime() {
        return firstHeartBeatTime;
    }

    public void setFirstHeartBeatTime(Date firstHeartBeatTime) {
        this.firstHeartBeatTime = firstHeartBeatTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getCsNo() {
        return csNo;
    }

    public void setCsNo(String csNo) {
        this.csNo = csNo;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
