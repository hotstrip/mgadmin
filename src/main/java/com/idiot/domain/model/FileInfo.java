package com.idiot.domain.model;

import java.util.Date;

/**
 * Created by idiot on 2016/12/13.
 */
public class FileInfo extends BaseBean {

    private static final long serialVersionUID = 1592738275720780979L;

    /** 文件信息编号 */
    private long fileId;
    /** 目录编号 */
    private long directoryId;
    /** 文件类型编号 */
    private long fileTypeId;
    /** 文件名称 */
    private String fileName;
    /** 目录路径 */
    private String directoryPath;
    /** 创建时间*/
    private Date createTime;
    /** 是否下发成功*/
    private int postFinish;
    /** 下发成功时间*/
    private Date postFinishTime;

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(long directoryId) {
        this.directoryId = directoryId;
    }

    public long getFileTypeId() {
        return fileTypeId;
    }

    public void setFileTypeId(long fileTypeId) {
        this.fileTypeId = fileTypeId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getPostFinish() {
        return postFinish;
    }

    public void setPostFinish(int postFinish) {
        this.postFinish = postFinish;
    }

    public Date getPostFinishTime() {
        return postFinishTime;
    }

    public void setPostFinishTime(Date postFinishTime) {
        this.postFinishTime = postFinishTime;
    }
}
