package com.idiot.domain.model;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
public class Directory extends BaseBean {

    private static final long serialVersionUID = 6747663811874637652L;

    /** 目录信息编号 */
    private long directoryId;
    /** 内容类型编号 */
    private long directoryTypeId;
    /** 目录名称 */
    private String directoryName;
    /** 是否根目录 */
    private int isRoot;
    /** 目录编码*/
    private String pathCode;
    /** 父级目录编号 */
    private long parentDirectoryId;

    private String pid;

    private List<Directory> directoryList;  //子目录

    private List<FileInfo> fileList;            //子文件

    public long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(long directoryId) {
        this.directoryId = directoryId;
    }

    public long getDirectoryTypeId() {
        return directoryTypeId;
    }

    public void setDirectoryTypeId(long directoryTypeId) {
        this.directoryTypeId = directoryTypeId;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public int getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(int isRoot) {
        this.isRoot = isRoot;
    }

    public String getPathCode() {
        return pathCode;
    }

    public void setPathCode(String pathCode) {
        this.pathCode = pathCode;
    }

    public long getParentDirectoryId() {
        return parentDirectoryId;
    }

    public void setParentDirectoryId(long parentDirectoryId) {
        this.parentDirectoryId = parentDirectoryId;
    }

    public String getPid() {
        return this.directoryId + "";
    }

    public List<Directory> getDirectoryList() {
        return directoryList;
    }

    public void setDirectoryList(List<Directory> directoryList) {
        this.directoryList = directoryList;
    }

    public List<FileInfo> getFileList() {
        return fileList;
    }

    public void setFileList(List<FileInfo> fileList) {
        this.fileList = fileList;
    }
}
