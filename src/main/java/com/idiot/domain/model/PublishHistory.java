package com.idiot.domain.model;

import java.util.Date;

/**
 * Created by idiot on 2016/12/29.
 */
public class PublishHistory extends BaseBean {
    private long publishHistoryId;
    private long fileId;
    private long agentId;
    private Date publishTime;

    public long getPublishHistoryId() {
        return publishHistoryId;
    }

    public void setPublishHistoryId(long publishHistoryId) {
        this.publishHistoryId = publishHistoryId;
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }
}
