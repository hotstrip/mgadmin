package com.idiot.domain.model;


/**
 * Created by idiot on 2016/12/13.
 */
public class FileType extends BaseBean {

    private static final long serialVersionUID = -8150796591270799243L;

    /** 内容类型编号 */
    private long fileTypeId;
    /** 类型名称 */
    private String fileTypeName;

    public long getFileTypeId() {
        return fileTypeId;
    }

    public void setFileTypeId(long fileTypeId) {
        this.fileTypeId = fileTypeId;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }
}
