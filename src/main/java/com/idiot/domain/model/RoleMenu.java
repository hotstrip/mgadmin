package com.idiot.domain.model;

/**
 * Created by idiot on 2016/12/13.
 */
public class RoleMenu extends BaseBean {

    private static final long serialVersionUID = 1580472366599171212L;

    /** 角色菜单编号 */
    private long roleMenuId;
    /** 角色编号 */
    private long roleId;
    /** 菜单编号 */
    private long menuId;

    public long getRoleMenuId() {
        return roleMenuId;
    }

    public void setRoleMenuId(long roleMenuId) {
        this.roleMenuId = roleMenuId;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }
}
