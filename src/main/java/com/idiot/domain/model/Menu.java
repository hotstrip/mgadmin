package com.idiot.domain.model;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
public class Menu extends BaseBean {

    private static final long serialVersionUID = 4864884039170821660L;

    /** 菜单编号 */
    private long menuId;
    /** 菜单名字 */
    private String menuName;
    /** 菜单父级编号 */
    private long parentId;
    /** 菜单链接地址 */
    private String menuUrl;
    /** 菜单图标 */
    private String menuIcon;
    /** 菜单显示顺序 */
    private int menuIndex;
    /** 权限 */
    private String permission;

    private String parentName;

    private boolean checked;        //是否选中

    private boolean chkDisabled;    //是否禁用

    private List<Menu> listMenus;	//方便存储多级菜单

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public int getMenuIndex() {
        return menuIndex;
    }

    public void setMenuIndex(int menuIndex) {
        this.menuIndex = menuIndex;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isChkDisabled() {
        return chkDisabled;
    }

    public void setChkDisabled(boolean chkDisabled) {
        this.chkDisabled = chkDisabled;
    }

    public List<Menu> getListMenus() {
        return listMenus;
    }

    public void setListMenus(List<Menu> listMenus) {
        this.listMenus = listMenus;
    }
}
