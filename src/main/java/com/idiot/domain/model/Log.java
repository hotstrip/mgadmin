package com.idiot.domain.model;

import java.util.Date;

/**
 * Created by idiot on 2016/12/13.
 */
public class Log extends BaseBean {

    private static final long serialVersionUID = 8749854651704588232L;

    /** 日志编号 */
    private long logId;
    /** 日志类型编号 */
    private long logTypeId;
    /** 用户编号 */
    private long userId;
    /** 操作时间 */
    private Date operateTime;
    /** 请求路径 */
    private String requestUrl;
    /** 响应的状态码 */
    private String responseCode;

    private String logTypeName;     //日志类型名称

    private String userName;        //操作用户      这两个作为显示

    public long getLogId() {
        return logId;
    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

    public long getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(long logTypeId) {
        this.logTypeId = logTypeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getLogTypeName() {
        return logTypeName;
    }

    public void setLogTypeName(String logTypeName) {
        this.logTypeName = logTypeName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
