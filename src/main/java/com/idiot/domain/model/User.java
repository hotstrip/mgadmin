package com.idiot.domain.model;

/**
 * Created by idiot on 2016/12/13.
 */
public class User extends BaseBean{

    private static final long serialVersionUID = -6187543379949815957L;

    /** 用户编号 */
    private long userId;
    /** 用户名称 */
    private String userName;
    /** 用户密码 */
    private String userPassword;
    /** 邮箱 */
    private String email;
    /** 用户目录 */
    private String userDirectory;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserDirectory() {
        return userDirectory;
    }

    public void setUserDirectory(String userDirectory) {
        this.userDirectory = userDirectory;
    }
}
