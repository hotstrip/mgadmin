package com.idiot.domain.model.enums;

/**
 * Created by idiot on 2016/12/17.
 */

/**
 * @description 状态枚举类
 */
public enum IsRootEnums {
    YES("yes",1), //是
    NO("no",0),//否
    ;

    private String name;
    private int value;

    IsRootEnums(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
