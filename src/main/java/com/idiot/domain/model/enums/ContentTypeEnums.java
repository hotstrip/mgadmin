package com.idiot.domain.model.enums;

/**
 * Created by idiot on 2016/12/17.
 */

/**
 * @description 内容类型枚举类
 */
public enum ContentTypeEnums {
    THEME("主题", 811177025686470656l),                  //主题
    COLUMN("栏目", 811177199129329664l),                 //栏目
    VIDEO("视频", 811177290506436608l),                  //视频
    PLAYLIST("播放列表", 811177401869402112l),           //播放列表
    LED("字幕", 811177514532601856l),                    //字幕
    SHIM("垫片", 811177633478868992l),                   //垫片
    ;

    private String name;
    private long value;

    ContentTypeEnums(String name, long value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
