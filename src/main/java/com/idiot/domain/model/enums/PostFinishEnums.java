package com.idiot.domain.model.enums;

/**
 * Created by idiot on 2016/12/17.
 */

/**
 * @description 下发是否成功枚举类（文件所处状态 ）
 */
public enum PostFinishEnums {
    SUCCESS("success",1), //成功
    FAILURE("failure",0),//失败
    UPLOAD("upload",2),//上传
    ;

    private String name;
    private int value;

    PostFinishEnums(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
