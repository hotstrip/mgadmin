package com.idiot.domain.model.enums;

/**
 * Created by idiot on 2016/12/17.
 */

/**
 * @description 日志类型枚举类
 */
public enum LogTypeEnums {
    LOGIN("login",808953751040688128l), //登录日志
    OPERTE("operate",808953871287189504l),//操作日志
    ;

    private String name;
    private long value;

    LogTypeEnums(String name, long value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    //根据name获取value
    public long getValueByName(String name){
        for(LogTypeEnums logTypeEnum : LogTypeEnums.values()){
            if(logTypeEnum.getName().equals(name)){
                return logTypeEnum.getValue();
            }
        }
        return 0;
    }
}
