package com.idiot.domain.model;

/**
 * Created by idiot on 2016/12/13.
 */
public class AgentGroup extends BaseBean {

    private static final long serialVersionUID = 2299468191869018160L;

    /** 终端分组编号 */
    private long agentGroupId;
    /** 终端分组名 */
    private String agentGroupName;

    public long getAgentGroupId() {
        return agentGroupId;
    }

    public void setAgentGroupId(long agentGroupId) {
        this.agentGroupId = agentGroupId;
    }

    public String getAgentGroupName() {
        return agentGroupName;
    }

    public void setAgentGroupName(String agentGroupName) {
        this.agentGroupName = agentGroupName;
    }
}
