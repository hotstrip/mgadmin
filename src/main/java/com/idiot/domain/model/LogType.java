package com.idiot.domain.model;

/**
 * Created by idiot on 2016/12/13.
 */
public class LogType extends BaseBean {

    private static final long serialVersionUID = 7010168067386073118L;

    /** 日志类型编号 */
    private long logTypeId;
    /** 日志类型名称 */
    private String typeName;

    public long getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(long logTypeId) {
        this.logTypeId = logTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
