package com.idiot.service;


import com.idiot.domain.model.LogType;

import java.util.List;

/**
 * Created by idiot on 2016/12/14.
 */
public interface ILogTypeService {
    //获取所有有效日志类型
    public List<LogType> getAllValidLogTypes();
}
