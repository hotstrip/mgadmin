package com.idiot.service;

import com.idiot.domain.model.TreeInfo;

import java.util.List;

/**
 * Created by idiot on 2016/12/20.
 */
public interface ITreeService {

    //获取菜单信息
    public List<TreeInfo> getMenuTree(long id, boolean role);

    //获取角色信息
    public List<TreeInfo> getRoleTree(long userId);
}
