package com.idiot.service;


import com.idiot.domain.model.Role;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
public interface IRoleService {
    //分页查询所有角色
    public List<Role> getAllRoles(RowBounds rowBounds);

    //新增
    public void insert(Role info);

    //修改
    public void update(Role info);

    //根据roleId获取角色信息
    public Role getRoleByRoleId(long roleId);

    //删除角色信息
    public void delete(long roleId);

}
