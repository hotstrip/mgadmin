package com.idiot.service;


import com.idiot.domain.model.Agent;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/23.
 */
public interface IAgentService {

    //新增
    public void insert(Agent info);

    //修改
    public void update(Agent info);

    //根据agentId获取终端信息
    public Agent getAgentByAgentId(long agentId);

    //删除
    public void delete(long agentId);

    //查询所有的终端信息
    public List<Agent> getAllAgents(RowBounds rowBounds);
}
