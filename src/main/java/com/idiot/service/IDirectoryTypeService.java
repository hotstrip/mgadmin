package com.idiot.service;

import com.idiot.domain.model.DirectoryType;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
public interface IDirectoryTypeService {
    //获取所有的内容类型
    public List<DirectoryType> getAllDirectoryTypes(RowBounds rowBounds);

    //获取内容类型信息
    public DirectoryType getDirectoryTypeByDirectoryTypeId(long contentTypeId);

    //新增
    public void insert(DirectoryType info);

    //修改
    public void update(DirectoryType info);

    //获取所有有效目录类型信息
    public List<DirectoryType> getAllValidDirectoryTypes();
}
