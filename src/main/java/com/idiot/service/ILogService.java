package com.idiot.service;

import com.github.pagehelper.Page;
import com.idiot.domain.model.Log;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2016/12/14.
 */
public interface ILogService {
    //新增日志信息
    public void addloginLog(Log log);

    //分页查询所有日志信息
    public Page<Log> getAllLogs(RowBounds rowBounds);

    //新增操作日志
    public void addOperateLog(Log log);

    //删除
    public void delete(long logId);
}
