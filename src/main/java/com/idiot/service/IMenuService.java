package com.idiot.service;

import com.github.pagehelper.Page;
import com.idiot.domain.model.Menu;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
public interface IMenuService {
    //根据userId获取权限
    public List<String> getPermissionsByUserId(long userId);

    //根据菜单parentId获取子菜单
    public List<Menu> getMenusByParenrId(long parentId);

    //获取该用户的一级菜单
    public List<Menu> getFirstLevelMenusByUserId(long userId);

    //获取所有菜单信息 分页
    public Page<Menu> getAllPageMenus(RowBounds rowBounds);

    //新增菜单信息
    public void insert(Menu info);

    //修改菜单信息
    public void update(Menu info);

    //根据menuId获取菜单信息
    public Menu getMenuByMenuId(long menuId);

    //删除菜单信息
    public void delete(long menuId);

    //获取所有有效的菜单信息  不分页
    public List<Menu> getAllMenus();

    //获取所有的一级菜单信息
    public List<Menu> getAllFirstMenus();
}
