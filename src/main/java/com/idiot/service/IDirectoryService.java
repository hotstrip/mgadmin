package com.idiot.service;



import com.idiot.domain.model.Directory;

import java.util.List;

/**
 * Created by idiot on 2016/12/22.
 */
public interface IDirectoryService {

    //获取所有有效的目录信息
    public List<Directory> getAllValidDirectories();

    //新增
    public void insert(Directory info);

    //修改
    public void update(Directory info);

    //根据parentDirectoryId获取目录信息
    public List<Directory> getDirectoryByParentDirectoryId(long parentDirectoryId);

    //根据目录 加载目录信息
    public Directory getDirectoryByDirectoryId(long directoryId);
}
