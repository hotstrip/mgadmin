package com.idiot.service.impl;

import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.common.utils.DateUtils;
import com.idiot.domain.common.utils.IdGen;
import com.idiot.domain.dao.IAgentDao;
import com.idiot.domain.model.Agent;
import com.idiot.service.IAgentService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/23.
 */
@Service
public class AgentServiceImpl implements IAgentService {
    @Resource
    private IAgentDao agentDao;

    @Override
    public void insert(Agent info) {
        info.setAgentId(IdGen.get().nextId());  //设置agentId
        info.setCreateTime(DateUtils.today());  //设置创建时间
        if(agentDao.insert(info) < 1){
            throw new MyException("新增终端信息失败");
        }
    }

    @Override
    public void update(Agent info) {
        if(agentDao.update(info) < 1){
            throw new MyException("修改终端信息失败");
        }
    }

    @Override
    public Agent getAgentByAgentId(long agentId) {
        return agentDao.getAgentByAgentId(agentId);
    }

    @Override
    public void delete(long agentId) {
        if(agentDao.delete(agentId) < 1){
            throw new MyException("删除终端信息失败");
        }
    }

    @Override
    public List<Agent> getAllAgents(RowBounds rowBounds) {
        return agentDao.getAllAgents(rowBounds);
    }
}
