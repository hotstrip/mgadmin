package com.idiot.service.impl;

import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.common.utils.DateUtils;
import com.idiot.domain.common.utils.IdGen;
import com.idiot.domain.dao.IFileDao;
import com.idiot.domain.model.FileInfo;
import com.idiot.domain.model.enums.PostFinishEnums;
import com.idiot.domain.model.enums.StatusEnums;
import com.idiot.service.IFileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/22.
 */
@Service
public class FileServiceImpl implements IFileService {
    @Resource
    private IFileDao fileDao;

    @Override
    public List<FileInfo> getFilesByDirectoryId(long parentDirectoryId) {
        return fileDao.getFilesByDirectoryId(parentDirectoryId);
    }

    @Override
    public void upload(FileInfo fileInfo) {
        fileInfo.setFileId(IdGen.get().nextId());   //设置文件编号
        fileInfo.setFileTypeId(1);                  //设置文件类型  目前是测试 随便给的一个类型编号
        fileInfo.setCreateTime(DateUtils.today());
        fileInfo.setStatus(StatusEnums.VALID.getValue());
        fileInfo.setPostFinish(PostFinishEnums.UPLOAD.getValue());     //上传
        if(fileDao.insert(fileInfo) < 1){
            throw new MyException("上传文件出错");
        }
    }
}
