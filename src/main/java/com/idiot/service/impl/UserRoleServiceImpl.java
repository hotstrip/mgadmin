package com.idiot.service.impl;


import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.dao.IUserRoleDao;
import com.idiot.domain.model.UserRole;
import com.idiot.service.IUserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by idiot on 2016/12/19.
 */
@Service
public class UserRoleServiceImpl implements IUserRoleService {
    @Resource
    private IUserRoleDao userRoleDao;

    @Override
    public void insert(UserRole userRole) {
        if(userRoleDao.insert(userRole) < 1){
            throw new MyException("新增用户角色关系失败");
        }
    }


    @Override
    public void remove(UserRole userRole) {
        UserRole info = userRoleDao.getUserRoleByUserRole(userRole);
        if(info != null){
            if(userRoleDao.delete(info.getUserRoleId()) < 1){
                throw new MyException("删除用户角色信息失败");
            }
        }else
            throw new MyException("该用户角色信息不存在");
    }
}
