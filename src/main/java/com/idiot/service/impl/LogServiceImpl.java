package com.idiot.service.impl;

import com.github.pagehelper.Page;

import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.common.utils.Constants;
import com.idiot.domain.common.utils.IdGen;
import com.idiot.domain.dao.ILogDao;
import com.idiot.domain.model.Log;
import com.idiot.domain.model.enums.LogTypeEnums;
import com.idiot.domain.model.enums.StatusEnums;
import com.idiot.service.ILogService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static org.apache.velocity.tools.generic.DateTool.getSystemDate;


/**
 * Created by idiot on 2016/12/14.
 */
@Service
public class LogServiceImpl implements ILogService {
    @Resource
    private ILogDao logDao;

    @Override
    public void addloginLog(Log log) {
        log.setLogId(IdGen.get().nextId());
        log.setOperateTime(getSystemDate());   //设置操作时间
        log.setStatus(StatusEnums.VALID.getValue());
        log.setResponseCode(Constants.ResponseCode.success + "");
        if(logDao.insert(log) < 1){
            throw new MyException("写入登录日志失败");
        }
    }

    @Override
    public Page<Log> getAllLogs(RowBounds rowBounds) {
        return logDao.getAllLogs(rowBounds);
    }

    @Override
    public void addOperateLog(Log log) {
        log.setLogId(IdGen.get().nextId());
        log.setOperateTime(getSystemDate());   //设置操作时间
        log.setLogTypeId(LogTypeEnums.OPERTE.getValue());   //设置logType
        log.setStatus(StatusEnums.VALID.getValue());
        if(logDao.insert(log) < 1){
            throw new MyException("写入操作日志失败");
        }
    }

    @Override
    public void delete(long logId) {
        if(logDao.delete(logId) < 1){
            throw new MyException("删除日志信息失败");
        }
    }
}
