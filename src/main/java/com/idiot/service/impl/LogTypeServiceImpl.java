package com.idiot.service.impl;


import com.idiot.domain.dao.ILogTypeDao;
import com.idiot.domain.model.LogType;
import com.idiot.service.ILogTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/14.
 */
@Service
public class LogTypeServiceImpl implements ILogTypeService {
    @Resource
    private ILogTypeDao logTypeDao;

    @Override
    public List<LogType> getAllValidLogTypes() {
        return logTypeDao.getAllValidLogTypes();
    }
}
