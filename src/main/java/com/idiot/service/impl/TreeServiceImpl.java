package com.idiot.service.impl;



import com.idiot.domain.dao.IMenuDao;
import com.idiot.domain.dao.IRoleDao;
import com.idiot.domain.model.Menu;
import com.idiot.domain.model.Role;
import com.idiot.domain.model.TreeInfo;
import com.idiot.service.ITreeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by idiot on 2016/12/20.
 */
@Service
public class TreeServiceImpl implements ITreeService {
    @Resource
    private IMenuDao menuDao;
    @Resource
    private IRoleDao roleDao;

    @Override
    public List<TreeInfo> getMenuTree(long roleId, boolean checked) {
        List<TreeInfo> list = new ArrayList<TreeInfo>();
        List<Menu> allMenus = menuDao.getAllMenus();
        //获取该角色对应的菜单信息
        List<Menu> rmenus = menuDao.getMenusByRoleId(roleId);
        for(Menu menu : allMenus){
            //创建tree
            TreeInfo menuTree = new TreeInfo();
            menuTree.setId(Long.toString(menu.getMenuId()));
            menuTree.setPid(Long.toString(menu.getParentId()));
            menuTree.setName(menu.getMenuName());
            //设置一级菜单是否选中   遍历该角色已经拥有的菜单
            for(Menu item : rmenus){
                if(menu.getMenuId() == item.getMenuId()){
                    menuTree.setChecked(true);
                }
            }
            menuTree.setChkDisabled(!checked);
            list.add(menuTree);
        }
        return list;
    }

    @Override
    public List<TreeInfo> getRoleTree(long userId) {
        List<TreeInfo> list = new ArrayList<TreeInfo>();
        List<Role> allValidRoless = roleDao.getAllValidRoles();
        //获取该用户对应的角色信息
        List<Role> uRoles = roleDao.getRolesByUserId(userId);
        for (Role role : allValidRoless){
            //创建tree
            TreeInfo roleTree = new TreeInfo();
            roleTree.setId(Long.toString(role.getRoleId()));
            roleTree.setName(role.getRoleName());
            roleTree.setRole(true);
            //设置角色是否选中   遍历该用户已经拥有的角色
            for(Role item : uRoles){
                if(role.getRoleId() == item.getRoleId()){
                    roleTree.setChecked(true);
                }
            }
            list.add(roleTree);
        }
        return list;
    }

}
