package com.idiot.service.impl;


import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.common.utils.IdGen;
import com.idiot.domain.dao.IAgentGroupDao;
import com.idiot.domain.model.AgentGroup;
import com.idiot.service.IAgentGroupService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
@Service
public class AgentGroupServiceImpl implements IAgentGroupService {
    @Resource
    private IAgentGroupDao agentGroupDao;

    @Override
    public List<AgentGroup> getAllAgentGroups(RowBounds rowBounds) {
        return agentGroupDao.getAllAgentGroups(rowBounds);
    }

    @Override
    public void insert(AgentGroup info) {
        info.setAgentGroupId(IdGen.get().nextId());  //设置agentGroupId
        if(agentGroupDao.insert(info) < 1){
            throw new MyException("新增终端分组信息失败");
        }
    }

    @Override
    public void update(AgentGroup info) {
        if(agentGroupDao.update(info) < 1){
            throw new MyException("修改终端分组信息失败");
        }
    }

    @Override
    public AgentGroup getAgentGroupByAgentGroupId(long agentGroupId) {
        return agentGroupDao.getAgentGroupByAgentGroupId(agentGroupId);
    }

    @Override
    public void delete(long agentGroupId) {
        if(agentGroupDao.delete(agentGroupId) < 1){
            throw new MyException("删除终端分组信息失败");
        }
    }

    @Override
    public List<AgentGroup> getAllValidAgentGroups() {
        return agentGroupDao.getAllValidAgentGroups();
    }
}
