package com.idiot.service.impl;

import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.common.utils.IdGen;
import com.idiot.domain.dao.IDirectoryDao;
import com.idiot.domain.model.Directory;
import com.idiot.domain.model.enums.IsRootEnums;
import com.idiot.service.IDirectoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/22.
 */
@Service
public class DirectoryServiceImpl implements IDirectoryService {
    @Resource
    private IDirectoryDao directoryDao;

    @Override
    public List<Directory> getAllValidDirectories() {
        return directoryDao.getAllValidDirectories();
    }

    @Override
    public void insert(Directory info) {
        info.setDirectoryId(IdGen.get().nextId());      //设置目录编号
        if(info.getParentDirectoryId() != 0){
            info.setIsRoot(IsRootEnums.NO.getValue());
        }else {
            info.setIsRoot(IsRootEnums.YES.getValue());
        }
        if(directoryDao.insert(info) < 1){
            throw new MyException("新增目录信息失败");
        }
    }

    @Override
    public void update(Directory info) {
        if(directoryDao.update(info) < 1){
            throw new MyException("修改目录信息失败");
        }
    }

    @Override
    public List<Directory> getDirectoryByParentDirectoryId(long parentDirectoryId) {
        return directoryDao.getDirectoryByParentDirectoryId(parentDirectoryId);
    }

    @Override
    public Directory getDirectoryByDirectoryId(long directoryId) {
        return directoryDao.getDirectoryByDirectoryId(directoryId);
    }
}
