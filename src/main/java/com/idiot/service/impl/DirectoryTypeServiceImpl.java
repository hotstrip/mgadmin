package com.idiot.service.impl;

import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.dao.IDirectoryTypeDao;
import com.idiot.domain.model.DirectoryType;
import com.idiot.service.IDirectoryTypeService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by idiot on 2016/12/26.
 * */
@Service
public class DirectoryTypeServiceImpl implements IDirectoryTypeService {
    @Resource
    private IDirectoryTypeDao directoryTypeDao;

    @Override
    public List<DirectoryType> getAllDirectoryTypes(RowBounds rowBounds) {
        return directoryTypeDao.getAllDirectoryTypes(rowBounds);
    }

    @Override
    public DirectoryType getDirectoryTypeByDirectoryTypeId(long directoryTypeId) {
        return directoryTypeDao.getDirectoryTypeByDirectoryTypeId(directoryTypeId);
    }

    @Override
    public void insert(DirectoryType info) {
        if(directoryTypeDao.insert(info) < 1){
            throw new MyException("新增目录类型信息失败");
        }
    }

    @Override
    public void update(DirectoryType info) {
        if(directoryTypeDao.update(info) < 1){
            throw new MyException("修改目录类型信息失败");
        }
    }

    @Override
    public List<DirectoryType> getAllValidDirectoryTypes() {
        return directoryTypeDao.getAllValidDirectoryTypes();
    }
}
