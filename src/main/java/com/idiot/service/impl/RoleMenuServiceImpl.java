package com.idiot.service.impl;


import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.dao.IRoleMenuDao;
import com.idiot.domain.model.RoleMenu;
import com.idiot.service.IRoleMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by idiot on 2016/12/19.
 */
@Service
public class RoleMenuServiceImpl implements IRoleMenuService {
    @Resource
    private IRoleMenuDao roleMenuDao;

    @Override
    public void insert(RoleMenu roleMenu) {
        if(roleMenuDao.insert(roleMenu) < 1){
            throw new MyException("新增角色菜单信息失败");
        }
    }

    @Override
    public void remove(RoleMenu roleMenu) {
        RoleMenu info = roleMenuDao.getRoleMenuByRoleMenu(roleMenu);
        if(info != null){
            if(roleMenuDao.delete(info.getRoleMenuId()) < 1){
                throw new MyException("删除角色菜单信息失败");
            }
        }else
            throw new MyException("删除角色菜单信息失败");
    }
}
