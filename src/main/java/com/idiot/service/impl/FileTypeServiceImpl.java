package com.idiot.service.impl;

import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.dao.IFileTypeDao;
import com.idiot.domain.model.FileType;
import com.idiot.service.IFileTypeService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by idiot on 2016/12/26.
 * */
@Service
public class FileTypeServiceImpl implements IFileTypeService {
    @Resource
    private IFileTypeDao fileTypeDao;

    @Override
    public List<FileType> getAllFileTypes(RowBounds rowBounds) {
        return fileTypeDao.getAllFileTypes(rowBounds);
    }

    @Override
    public FileType getFileTypeByFileTypeId(long fileTypeId) {
        return fileTypeDao.getFileTypeByFileTypeId(fileTypeId);
    }

    @Override
    public void insert(FileType info) {
        if(fileTypeDao.insert(info) < 1){
            throw new MyException("新增文件类型信息失败");
        }
    }

    @Override
    public void update(FileType info) {
        if(fileTypeDao.update(info) < 1){
            throw new MyException("修改文件类型信息失败");
        }
    }
}
