package com.idiot.service.impl;


import com.idiot.domain.common.exceptions.MyException;
import com.idiot.domain.common.utils.IdGen;
import com.idiot.domain.dao.IRoleDao;
import com.idiot.domain.dao.IRoleMenuDao;
import com.idiot.domain.model.Role;
import com.idiot.domain.model.RoleMenu;
import com.idiot.service.IRoleService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
@Service
public class RoleServiceImpl implements IRoleService {
    private static Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);
    @Resource
    private IRoleDao roleDao;
    @Resource
    private IRoleMenuDao roleMenuDao;

    @Override
    public List<Role> getAllRoles(RowBounds rowBounds) {
        return roleDao.getAllRoles(rowBounds);
    }

    @Override
    public void insert(Role info) {
        info.setRoleId(IdGen.get().nextId());   //设置roleId
        if(roleDao.insert(info) < 1){
            throw new MyException("新增角色失败");
        }
    }

    @Override
    public void update(Role info) {
        if(roleDao.update(info) < 1){
            throw new MyException("新增角色失败");
        }
    }

    @Override
    public Role getRoleByRoleId(long roleId) {
        return roleDao.getRoleByRoleId(roleId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(long roleId) {
        //删除角色信息   以及绑定的角色菜单信息
        if(roleDao.delete(roleId) < 1){
            logger.error("删除角色信息失败");
        }
        //查询该角色所拥有的菜单
        List<RoleMenu> list = roleMenuDao.getRoleMenusByRoleId(roleId);
        if(list != null && list.size() > 0) {
            if (roleMenuDao.deleteRoleMenusByRoleId(roleId) < 1) {
                logger.error("删除角色菜单关系信息失败");
            }
        }
    }

}
