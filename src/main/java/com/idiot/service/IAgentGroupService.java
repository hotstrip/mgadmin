package com.idiot.service;

import com.idiot.domain.model.AgentGroup;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
public interface IAgentGroupService {
    //分页查询所有终端分组
    public List<AgentGroup> getAllAgentGroups(RowBounds rowBounds);

    //新增
    public void insert(AgentGroup info);

    //修改
    public void update(AgentGroup info);

    //根据agentGroupId获取终端分组信息
    public AgentGroup getAgentGroupByAgentGroupId(long agentGroupId);

    //删除
    public void delete(long agentGroupId);

    //获取所有有效的终端分组
    public List<AgentGroup> getAllValidAgentGroups();
}
