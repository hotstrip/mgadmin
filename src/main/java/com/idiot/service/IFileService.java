package com.idiot.service;


import com.idiot.domain.model.FileInfo;

import java.util.List;

/**
 * Created by idiot on 2016/12/22.
 */
public interface IFileService {

    //获取目录下的文件信息
    public List<FileInfo> getFilesByDirectoryId(long parentDirectoryId);

    //上传文件
    public void upload(FileInfo fileInfo);
}
