package com.idiot.service;

import com.idiot.domain.model.FileType;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
public interface IFileTypeService {
    //获取所有的内容类型
    public List<FileType> getAllFileTypes(RowBounds rowBounds);

    //获取内容类型信息
    public FileType getFileTypeByFileTypeId(long fileTypeId);

    //新增
    public void insert(FileType info);

    //修改
    public void update(FileType info);
}
