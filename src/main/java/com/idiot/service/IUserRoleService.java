package com.idiot.service;


import com.idiot.domain.model.UserRole;

/**
 * Created by idiot on 2016/12/19.
 */
public interface IUserRoleService {
    //新增
    public void insert(UserRole userRole);

    //移除userRole
    public void remove(UserRole userRole);
}
