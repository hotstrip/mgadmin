package com.idiot.service;

import com.idiot.domain.model.RoleMenu;

/**
 * Created by idiot on 2016/12/19.
 */
public interface IRoleMenuService {
    //新增
    public void insert(RoleMenu roleMenu);

    //删除
    public void remove(RoleMenu roleMenu);
}
