package com.idiot.service;

import com.github.pagehelper.Page;
import com.idiot.domain.model.User;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
public interface IUserService {
    //根据用户名查询用户信息
    public User getUserByName(String userName);

    //获取所有的管理员用户
    public Page<User> getAllAdminUsers(RowBounds rowBounds);

    //新增用户信息
    public void insert(User info);

    //修改用户信息
    public void update(User info);

    //根据userId获取用户信息
    public User getUserByUserId(long userId);

    //删除用户信息
    public void delete(long userId);

    //获取所有的有效用户
    public List<User> getAllUsers();
}
