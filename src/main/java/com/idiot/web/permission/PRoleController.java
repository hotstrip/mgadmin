package com.idiot.web.permission;

import com.idiot.domain.common.utils.ResponseUtils;
import com.idiot.domain.model.Role;
import com.idiot.web.base.SuperController;
import com.idiot.service.IRoleService;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description 权限管理
 * Created by idiot on 2016/12/15.
 */
@Controller
@RequestMapping(value = "permission/role")
public class PRoleController extends SuperController {
    private static final Logger logger = LoggerFactory.getLogger(PRoleController.class);

    @Resource
    private IRoleService roleService;

    /**
     * 权限之角色相关
     * */

    //加载page页面
    @RequiresPermissions(value = "permission:role:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "permission/role/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Role info, Model model){
        List<Role> lists = roleService.getAllRoles(new RowBounds((pageNo-1)*pageSize, pageSize));
        model.addAttribute("lists", lists);
        return "permission/role/data";
    }

    //加载角色信息编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        return "permission/role/update";
    }

    //编辑角色信息
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(Role info){
        //userId为空/0时表示   新增操作
        if(0 == info.getRoleId()){
            roleService.insert(info);
            ResponseUtils.writeSuccessReponse(request, response, "新增角色信息成功");
        }else {     //修改操作
            roleService.update(info);
            ResponseUtils.writeSuccessReponse(request, response, "修改角色信息成功");
        }
    }

    //加载编辑页面
    @RequestMapping(value = "{roleId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("roleId") long roleId, Model model){
        Role role = roleService.getRoleByRoleId(roleId);
        model.addAttribute("role",role);
        return "permission/role/update";
    }

    //删除角色信息
    @RequestMapping(value = "{roleId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("roleId") long roleId){
        roleService.delete(roleId);
        ResponseUtils.writeSuccessReponse(request,response,"删除角色信息成功");
    }
}
