package com.idiot.web.permission;

import com.github.pagehelper.Page;
import com.idiot.domain.common.utils.ResponseUtils;
import com.idiot.domain.model.User;
import com.idiot.web.base.SuperController;
import com.idiot.service.IUserService;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

import static com.alibaba.fastjson.JSON.toJSON;

/**
 * @description 权限管理
 * Created by idiot on 2016/12/15.
 */
@Controller
@RequestMapping(value = "permission/user")
public class PUserController extends SuperController {
    private static final Logger logger = LoggerFactory.getLogger(PUserController.class);

    @Resource
    private IUserService userService;

    /**
     * 权限之用户相关
     * */
    //加载page页面
    @RequiresPermissions(value = "permission:user:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "permission/user/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, User info, Model model){
        Page<User> lists = userService.getAllAdminUsers(new RowBounds((pageNo-1)*pageSize, pageSize));
        //JSONArray jsonArray = (JSONArray) JSON.toJSON(lists);
        model.addAttribute("lists", lists);
        //model.addAttribute("items", jsonArray);
        return "permission/user/data";
    }

    //加载用户信息编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        return "permission/user/update";
    }

    //编辑用户信息
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(User info){
        //userId为空/0时表示   新增操作
        if(0 == info.getUserId()){
            userService.insert(info);
            ResponseUtils.writeSuccessReponse(request, response, "新增用户信息成功");
        }else {     //修改操作
            userService.update(info);
            ResponseUtils.writeSuccessReponse(request, response, "修改用户信息成功");
        }
    }

    //加载编辑页面
    @RequestMapping(value = "{userId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("userId") long userId, Model model){
        User user = userService.getUserByUserId(userId);
        model.addAttribute("user",user);
        return "permission/user/update";
    }

    //删除用户信息
    @RequestMapping(value = "{userId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("userId") long userId){
        userService.delete(userId);
        ResponseUtils.writeSuccessReponse(request,response,"删除用户信息成功");
    }

}
