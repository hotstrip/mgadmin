package com.idiot.web.permission;

import com.alibaba.fastjson.JSON;
import com.idiot.domain.common.utils.IdGen;
import com.idiot.domain.common.utils.ResponseUtils;
import com.idiot.web.base.SuperController;
import com.idiot.domain.model.*;
import com.idiot.domain.model.enums.StatusEnums;
import com.idiot.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description 权限管理
 * Created by idiot on 2016/12/15.
 */
@Controller
@RequestMapping(value = "permission/grant")
public class PGrantController extends SuperController {
    private static final Logger logger = LoggerFactory.getLogger(PGrantController.class);

    @Resource
    private IUserService userService;
    @Resource
    private IUserRoleService userRoleService;
    @Resource
    private IRoleMenuService roleMenuService;
    @Resource
    private ITreeService treeService;
    /**
     * 权限之授权相关
     * */

    //加载page页面
    @RequiresPermissions(value = "permission:grant:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        List<User> lists = userService.getAllUsers();
        model.addAttribute("lists",lists);
        return "permission/grant/page";
    }

    //加载角色信息
    @RequestMapping(value = "showResources", method = RequestMethod.POST)
    public String showResources(Model model, long userId){
        List<TreeInfo> list = treeService.getRoleTree(userId);
        String json = JSON.toJSONString(list);
        model.addAttribute("jsonData",json);
        return "permission/grant/resources";
    }

    //加载资源信息（菜单）
    @RequestMapping(value = "loadResources", method = RequestMethod.POST)
    public void loadResources(Model model, long roleId, boolean checked){
        List<TreeInfo> list = treeService.getMenuTree(roleId,checked);
        ResponseUtils.writeSuccessReponse(request,response,list);
    }

    //修改用户角色信息
    @RequestMapping(value = "updateUserRole", method = RequestMethod.POST)
    public void updateUserRole(Model model, long userId, long roleId, boolean checked){
        //checked为true  表示新增
        UserRole userRole = new UserRole();
        userRole.setUserId(userId);
        userRole.setRoleId(roleId);
        if(checked){
            userRole.setUserRoleId(IdGen.get().nextId());
            userRole.setStatus(StatusEnums.VALID.getValue());
            userRoleService.insert(userRole);
            ResponseUtils.writeSuccessReponse(request,response,"新增用户角色信息成功");
        }else {
            userRoleService.remove(userRole);
            ResponseUtils.writeSuccessReponse(request,response,"删除用户角色信息成功");
        }
    }

    //修改角色菜单信息
    @RequestMapping(value = "updateRoleMenu", method = RequestMethod.POST)
    public void updateRoleMenu(Model model, long menuId, long roleId, boolean checked){
        //checked为true  表示新增
        RoleMenu roleMenu= new RoleMenu();
        roleMenu.setMenuId(menuId);
        roleMenu.setRoleId(roleId);
        if(checked){
            roleMenu.setRoleMenuId(IdGen.get().nextId());
            roleMenu.setStatus(StatusEnums.VALID.getValue());
            roleMenuService.insert(roleMenu);
            ResponseUtils.writeSuccessReponse(request,response,"新增角色菜单信息成功");
        }else {
            roleMenuService.remove(roleMenu);
            ResponseUtils.writeSuccessReponse(request,response,"删除角色菜单信息成功");
        }
    }

}
