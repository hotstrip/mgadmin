package com.idiot.web.user;

import com.idiot.domain.common.utils.EncodeMD5;
import com.idiot.domain.common.utils.ResponseUtils;
import com.idiot.web.base.SuperController;
import com.idiot.domain.model.User;
import com.idiot.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * Created by idiot on 2016/12/13.
 */
@Controller
@RequestMapping(value = "user")
public class UserController extends SuperController{
    @Resource
    private IUserService userService;


    //加载page页面
    @RequiresPermissions(value = "user:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        User user = getUser();
        if(user != null){
            model.addAttribute("user",user);
        }
        return "user/page";
    }

    //修改用户密码
    @RequestMapping(value = "updatePassword", method = RequestMethod.POST)
    public void updatePassword(String oldPassword, long userId, User info){
        User user = userService.getUserByUserId(userId);
        if(user != null){
            if(EncodeMD5.GetMD5Code(oldPassword).equals(user.getUserPassword())){
                info.setUserPassword(EncodeMD5.GetMD5Code(info.getUserPassword())); //加密密码
                userService.update(info);
                ResponseUtils.writeSuccessReponse(request,response,"修改用户密码成功");
            }
            ResponseUtils.writeErrorResponse(request,response,"原密码错误");
        }
        ResponseUtils.writeErrorResponse(request,response,"修改用户密码失败");
    }

}
