package com.idiot.web.cloudDisk;

import com.idiot.domain.common.utils.ResponseUtils;
import com.idiot.domain.model.DirectoryType;
import com.idiot.domain.model.FileType;
import com.idiot.web.base.SuperController;
import com.idiot.service.IDirectoryTypeService;
import com.idiot.service.IFileTypeService;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/20.
 * @description 管理目录
 */
@Controller
@RequestMapping(value = "cloudDisk/contentType")
public class ContentTypeController extends SuperController {

    @Resource
    private IDirectoryTypeService directoryTypeService;
    @Resource
    private IFileTypeService fileTypeService;

    //加载page页面
    @RequiresPermissions(value = "cloudDisk:contentType:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        return "cloudDisk/contentType/page";
    }

    //加载内容类型数据
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Model model, Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, String contentType){
        if("directory".equals(contentType)){
            //加载目录类型数据
            List<DirectoryType> directoryTypeList = directoryTypeService.getAllDirectoryTypes(new RowBounds((pageNo-1)*pageSize, pageSize));
            model.addAttribute("lists",directoryTypeList);
            model.addAttribute("isDirectory", true);    //标识是目录列表
        }else if("file".equals(contentType)){
            //加载文件类型数据
            List<FileType> fileTypeList = fileTypeService.getAllFileTypes(new RowBounds((pageNo-1)*pageSize, pageSize));
            model.addAttribute("lists",fileTypeList);
        }
        return "cloudDisk/contentType/data";
    }

    //编辑目录类型信息
    @RequestMapping(value = "directory/update", method = RequestMethod.POST)
    public void updateDirectory(DirectoryType info){
        //directoryTypeId为空/0时表示   新增操作
        if(0 == info.getDirectoryTypeId()){
            directoryTypeService.insert(info);
            ResponseUtils.writeSuccessReponse(request, response, "新增目录类型信息成功");
        }else {     //修改操作
            directoryTypeService.update(info);
            ResponseUtils.writeSuccessReponse(request, response, "修改目录类型信息成功");
        }
    }

    //编辑文件类型信息
    @RequestMapping(value = "file/update", method = RequestMethod.POST)
    public void updateFile(FileType info){
        //fileTypeId为空/0时表示   新增操作
        if(0 == info.getFileTypeId()){
            fileTypeService.insert(info);
            ResponseUtils.writeSuccessReponse(request, response, "新增文件类型信息成功");
        }else {     //修改操作
            fileTypeService.update(info);
            ResponseUtils.writeSuccessReponse(request, response, "修改文件类型信息成功");
        }
    }

    //加载内容类型编辑页面
    @RequestMapping(value = "{type}/toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model, @PathVariable("type")String type){
        if("directory".equals(type)){
            return "cloudDisk/contentType/directoryUpdate";
        }else if("file".equals(type)){
            return "cloudDisk/contentType/fileUpdate";
        }else
            return "error";
    }

    //加载内容类型编辑页面
    @RequestMapping(value = "{type}/toUpdate/{typeId}", method = RequestMethod.GET)
    public String toUpdate(Model model, @PathVariable("type")String type, @PathVariable("typeId")long typeId){
        if("directory".equals(type)){
            DirectoryType directoryType = directoryTypeService.getDirectoryTypeByDirectoryTypeId(typeId);
            model.addAttribute("directoryType", directoryType);
            return "cloudDisk/contentType/directoryUpdate";
        }else if("file".equals(type)){
            FileType fileType = fileTypeService.getFileTypeByFileTypeId(typeId);
            model.addAttribute("fileType", fileType);
            return "cloudDisk/contentType/fileUpdate";
        }else
            return "error";
    }
}
