package com.idiot.web.release;

import com.idiot.web.base.SuperController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by idiot on 2016/12/29.
 */
@Controller
@RequestMapping(value = "release/push")
public class ReleasePushController extends SuperController{


    //加载page页面
    @RequiresPermissions(value = "release:push:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "release/push/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Model model){
        return "release/push/data";
    }
}
