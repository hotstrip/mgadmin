package com.idiot.web.agent;

import com.idiot.domain.common.utils.ResponseUtils;
import com.idiot.domain.model.Agent;
import com.idiot.domain.model.AgentGroup;
import com.idiot.service.IAgentGroupService;
import com.idiot.service.IAgentService;
import com.idiot.web.base.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by idiot on 2016/12/23.
 * @description 终端列表信息
 */
@Controller
@RequestMapping(value = "agent/agentList")
public class AgentListController extends SuperController {
    @Resource
    private IAgentService agentService;
    @Resource
    private IAgentGroupService agentGroupService;

    //加载终端列表页面
    @RequiresPermissions("agent:agentList:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "agent/agentList/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Model model, Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Agent info){
        //从数据库获取终端列表信息
        List<Agent> list = agentService.getAllAgents(new RowBounds((pageNo-1)*pageSize, pageSize));
        model.addAttribute("lists",list);
        return "agent/agentList/data";
    }

    //加载终端编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        List<AgentGroup> agentGroups = agentGroupService.getAllValidAgentGroups();
        model.addAttribute("agentGroups", agentGroups);
        return "agent/agentList/update";
    }

    //编辑终端信息
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(Agent info){
        //agentId为空/0时表示   新增操作
        if(0 == info.getAgentId()){
            agentService.insert(info);
            ResponseUtils.writeSuccessReponse(request, response, "新增终端信息成功");
        }else {     //修改操作
            agentService.update(info);
            ResponseUtils.writeSuccessReponse(request, response, "修改终端信息成功");
        }
    }

    //加载编辑页面
    @RequestMapping(value = "{agentId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("agentId") long agentId, Model model){
        Agent agent = agentService.getAgentByAgentId(agentId);
        List<AgentGroup> agentGroups = agentGroupService.getAllValidAgentGroups();
        model.addAttribute("agent",agent);
        model.addAttribute("agentGroups", agentGroups);
        return "agent/agentList/update";
    }

    //删除终端信息
    @RequestMapping(value = "{agentId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("agentId") long agentId){
        agentService.delete(agentId);
        ResponseUtils.writeSuccessReponse(request,response,"删除终端信息成功");
    }
}
