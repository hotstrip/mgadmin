package com.idiot.web.agent;

import com.idiot.domain.common.utils.ResponseUtils;
import com.idiot.domain.model.Agent;
import com.idiot.domain.model.AgentGroup;
import com.idiot.web.base.SuperController;
import com.idiot.service.IAgentGroupService;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/23.
 * @description 终端分组信息
 */
@Controller
@RequestMapping(value = "agent/agentGroup")
public class AgentGroupController extends SuperController {
    @Resource
    private IAgentGroupService agentGroupService;

    //加载终端列表页面
    @RequiresPermissions("agent:agentGroup:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "agent/agentGroup/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Model model, Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Agent info){
        //从数据库获取终端分组列表信息
        List<AgentGroup> list = agentGroupService.getAllAgentGroups(new RowBounds((pageNo-1)*pageSize, pageSize));
        model.addAttribute("lists",list);
        return "agent/agentGroup/data";
    }

    //加载终端分组编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        return "agent/agentGroup/update";
    }

    //编辑终端分组信息
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(AgentGroup info){
        //agentId为空/0时表示   新增操作
        if(0 == info.getAgentGroupId()){
            agentGroupService.insert(info);
            ResponseUtils.writeSuccessReponse(request, response, "新增终端分组信息成功");
        }else {     //修改操作
            agentGroupService.update(info);
            ResponseUtils.writeSuccessReponse(request, response, "修改终端分组信息成功");
        }
    }

    //加载编辑页面
    @RequestMapping(value = "{agentGroupId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("agentGroupId") long agentGroupId, Model model){
        AgentGroup agentGroup = agentGroupService.getAgentGroupByAgentGroupId(agentGroupId);
        model.addAttribute("agentGroup",agentGroup);
        return "agent/agentGroup/update";
    }

    //删除终端分组信息
    @RequestMapping(value = "{agentGroupId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("agentGroupId") long agentGroupId){
        agentGroupService.delete(agentGroupId);
        ResponseUtils.writeSuccessReponse(request,response,"删除终端信息成功");
    }
}
