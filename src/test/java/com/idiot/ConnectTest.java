package com.idiot;

import com.idiot.service.IMenuService;
import com.idiot.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by idiot on 2016/12/13.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ConnectTest {
    private final static Logger logger = LoggerFactory.getLogger(ConnectTest.class);

    @Resource
    private IUserService userService;
    @Resource
    private IMenuService menuService;

    @Test
    public void connect(){
        logger.info("测试");
        userService.getUserByName("idiot");
        //menuService.getMenusByUserId(123456);
    }
}
