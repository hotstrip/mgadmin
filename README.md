#平台2.1
----------
项目结构：

>```
mgadmin
    --src
        --main
            --java
                --com.idiot
                    --domain
                        --common    #一些配置类  工具类
                        --dao
                        --mapper    #mybatis管理sql  xml
                        --model
                    --service    #业务处理
                    --web(controller)
                --Application.java  #项目启动类
            --resources
        --test
    --target
    --pom.xml
```

##简介
--------

>关于后端框架
* 该项目使用spring boot为主要框架
* mybatis操作数据库
* 集成shiro权限框架
* velocity模板引擎（尽管该引擎在spring boot1.4X以上已经弃用）
* kaptha验证码....
* 对了，差点忘了  还有个webuploader实现的大文件分片上传，路径在resources/templates/cloudDisk/content/upload.vm;使用时记得修改默认路径，如下：

```
var uploader = new WebUploader.Uploader({
        swf: 'webuploader/swf/Uploader.swf',
        server: '/cloudDisk/content/file/upload',
        pick: '#picker',
        method: 'post',
        formData: {
        uploadPath: "E://home/",
        "directoryId": directoryId
        },
        threads: 1,//上传并发数
        chunked: true   // 开起分片上传
    });
```
    重点是修改uploadPath.....


--------

>关于前端ui
* H-ui-admin
* layerPage分页
* .....

--------

>说这么多，都是废话，简单的讲就是一个shiro权限框架案例
>想知道更多请加群298956591(该群只能邀请加入  请联系1129187849)
>这个是测试网站 [点击进入](http://idiotalex.com/index.html)   用户名：alex  密码 123456  普通管理员
>网站已关闭，暂时失效
>也同样欢迎捐赠
>项目完结