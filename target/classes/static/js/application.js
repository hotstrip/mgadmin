/***
 * 封装常用  以及  模块所用的方法
 * */

/**通用方法*/
var common = {
    /**
     * @description  获取数据
     * @param _type 提交类型
     * @param _url  请求地址
     * @param _data 请求数据
     * @param _dataType 返回类型
     * @param _element html元素
     */
    getData: function (_type, _url, _data, _dataType, _element) {
        $.ajax({
            type: _type,
            url: _url,
            data: _data,
            dataTyp: _dataType,
            error: function(){
                return false;
            },
            success: function(data){
                _element.html(data);
            }
        })
    },
    /**
     * @description 更换图片  验证码
     * obj html元素
     * */
    changeImg: function(obj) {
        var src = $(obj).attr("src")
        src += '?p=' + Math.random();
        $(obj).attr("src", src)
    },
    /**
     * @description  列表删除元素
     * msg：提示消息
     * url：请求路径
     * */
    deleteMethod: function(msg,url){
        var index = layer.open({
            content: msg,
            btn: ['确认', '取消'],
            shadeClose: false,
            yes: function(){
                $.post(url,{},function (result) {
                    layer.msg(result.data, {icon: 6, time: 2000}, function(){
                        fresh();
                        layer.close(index);
                    })
                },"json")
            }, no: function(){
                layer.close(index)
            }
        });
    },
    /**
     * @description 加载数据到指定页面位置
     * type: 提交方式
     * url: 请求路径
     * data: 数据 json格式
     * dataType: 返回数据类型
     * pageElement 页面元素 jquery对象
     * */
    dataToHtml: function(type, url, data, dataType, $pageElement){
        $.ajax({
            type: type,
            url: url,
            data: data,
            dataTyp: dataType,
            error: function(){
                return false;
            },
            success: function(data){
                $pageElement.html(data);
            }
        })
    },
    /**
     * @description post提交  返回信息
     * url: 请求路径
     * data: 提交数据
     * */
    postRefresh: function (url, data){
        $.post(url, data, function(result){
            layer.msg(result.data, {icon: 6, time: 2000}, function(){
                parent.fresh();
                layer_close();
            })
        },"json");
    },
    /**
     * @description post提交数据 返回json信息
     * url: 请求路径
     * data: 请求数据
     * */
    postToJson: function(url, data){
        $.post(url, data, function (result) {
            layer.msg(result.data, {icon: 6, time: 1000});
        },"json")
    }

}

/**角色模块*/
var roleUtils = {

}

/**菜单模块*/
var menuUtils = {

}